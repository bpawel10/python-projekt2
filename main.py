import argparse
import sys
import json
import tkinter
from ast import literal_eval
from PIL import Image, ImageDraw, ImageTk

def resolveColor(data, color):
    color = data['Palette'][color] if color in data['Palette'] else color

    if color[0] == '(':
        color = literal_eval(color)

    return color

def getFigureColor(data, figure):
    return resolveColor(data, figure['color']) if 'color' in figure else data['Screen']['fg_color']

def main():
    parser = argparse.ArgumentParser(add_help = False)
    parser.add_argument('input', type = str)
    parser.add_argument('-o', '--output', nargs = '?', type = str)
    args = parser.parse_args()

    try:
        with open(args.input) as f:
            data = json.load(f)
    except IOError:
        print('Failed to open input file')
        sys.exit(1)
    except ValueError:
        print('Failed to decode input file')
        sys.exit(2)
    except:
        print('Something went wrong while processing input file')
        sys.exit(3)

    width = data['Screen']['width']
    height = data['Screen']['height']
    img = Image.new('RGB', (width, height), resolveColor(data, data['Screen']['bg_color']))

    if len(data['Figures']) > 0:
        draw = ImageDraw.Draw(img)

        for figure in data['Figures']:
            if figure['type'] == 'point':
                draw.point([(figure['x'], figure['y'])], getFigureColor(data, figure))
            elif figure['type'] == 'polygon':
                draw.polygon(list(map(lambda point: tuple(point), figure['points'])), getFigureColor(data, figure))
            elif figure['type'] == 'rectangle':
                draw.rectangle([figure['x'], figure['y'], figure['x']+figure['width'], figure['y']+figure['height']], getFigureColor(data, figure))
            elif figure['type'] == 'square':
                if 'size' in figure:
                    draw.rectangle([figure['x']-figure['size']/2, figure['y']-figure['size']/2, figure['x']+figure['size']/2, figure['y']+figure['size']/2], getFigureColor(data, figure))
                elif 'radius' in figure:
                    draw.ellipse([figure['x']-figure['radius'], figure['y']-figure['radius'], figure['x']+figure['radius'], figure['y']+figure['radius']], getFigureColor(data, figure))

    if args.output:
        img.save(args.output, 'PNG')

    root = tkinter.Tk()
    cv = tkinter.Canvas(root, width = width, height = height, highlightthickness = 0)
    photo = ImageTk.PhotoImage(img)
    cv.create_image(width/2, height/2, image = photo)
    cv.pack()
    root.mainloop()

if __name__ == "__main__":
    main()
